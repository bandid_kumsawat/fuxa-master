import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class EndPointApi {
    private static url: string = null;

    public static getURL() {
        if (!this.url) {
            if (environment.apiEndpoint) {
                this.url = environment.apiEndpoint;
            } else {
                const origin = location.origin;
                let path = location.origin.split('/')[2];
                const protocoll = location.origin.split(':')[0];
                const temp = path.split(':')[0];
                if (temp.length > 1 && environment.apiPort) {
                    path = "nrwiot.pwa.co.th/fuxa"//temp //+ ':' + environment.apiPort;
                    // path = "localhost:1881"
                }
                this.url = 'https' + '://' + path;
            }
        }
        console.log("getURL")
        console.log(this.url)
        // this.url = "nrwiot.pwa.co.th/fuxa";
        return this.url;
    }

    public static getRemoteURL(destIp: string) {
        const protocoll = location.origin.split(':')[0];
        const path = destIp + ':' + environment.apiPort;
        console.log("getRemoteURL")
        console.log(protocoll + '://' + path + '')

        // return protocoll + '://' + path + '/api';
        // return "http://nrwiot.pwa.co.th/fuxa" + "/api"

    }
}